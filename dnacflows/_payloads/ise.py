class ISE(object):
	def __init__(self):
		super(ISE, self).__init__()

	def new_server(self,iseip,iseuname,isepassword,isefqdn,secret,subscrinername):
		"""

		:param iseip:
		:param iseuname:
		:param isepassword:
		:param isefqdn:
		:param secret:
		:param subscrinername:
		:return: Payload for creating a new ISE server
		"""
		payload = ({

			"ipAddress":          iseip,
			"sharedSecret":       secret,
			"role":               "PRIMARY",
			"protocol":           "RADIUS_TACACS",
			"port":               "49",
			"authenticationPort": "1812",
			"accountingPort":     "1813",
			"retries":            "3",
			"timeoutSeconds":     "4",
			"isIseEnabled":       True,
			"ciscoISEUrl":        "null",
			"ciscoIseDtos":       [
				{
					"description":    "",
					"userName":       iseuname,
					"password":       isepassword,
					"fqdn":           isefqdn,
					"subscriberName": subscrinername,
					"ipAddress":      iseip,
					"sshkey":         ""
				}
			]
		})
		return payload

