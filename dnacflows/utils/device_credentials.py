from dnacflows import _payloads


class Credentials:
	def __init__(self,dnac):
		super(Credentials,self).__init__()
		self._dnac = dnac
		
	def cli(self,desc,username,password,enablepassword):
		"""


		:param desc:
		:param username:
		:param password:
		:param enablepassword:
		:return:
		"""
		self._dnac.custom_caller.create_device_credentials(payload=_payloads.credentials.cli(desc, username, password, enablepassword))


	def snmpv2read(self,desc, readcommunity):
		"""

		:param desc:
		:param readcommunity:
		:return:
		"""

		self._dnac.custom_caller.create_device_credentials(payload=_payloads.credentials.snmpv2read(desc, readcommunity))

	def snmpv2write(self, desc, writecommunity):
		"""

		:param desc: Description
		:param writecommunity:
		:return: Payload for creating device snmpV2 write credentails
		"""

		self._dnac.custom_caller.create_device_credentials(
			payload=_payloads.credentials.snmpv2read(desc, writecommunity))

	def snmpv3(self, desc, username,privacyType,privacyPassword,authType,authPassword,snmpMode):
		"""


		:param desc:
		:param username:
		:param privacyType:
		:param privacyPassword:
		:param authType:
		:param authPassword:
		:param snmpMode:
		:return:
		"""
		self._dnac.custom_caller.create_device_credentials(
			payload=_payloads.credentials.snmpv3(desc, username, privacyType, privacyPassword, authType, authPassword, snmpMode))

	def httpsRead(self,name, username, password, port):
		"""

		:param name:
		:param username:
		:param password:
		:param port:
		:return:
		"""
		self._dnac.custom_caller.create_device_credentials(
			payload=_payloads.credentials.httpsRead(name, username, password, port)
		)

	def httpswrite(self,name, username, password, port):
		"""


		:param name:
		:param username:
		:param password:
		:param port:
		:return:
		"""
		self._dnac.custom_caller.create_device_credentials(
			payload=_payloads.credentials.httpsRead(name, username,
													password, port)
		)

