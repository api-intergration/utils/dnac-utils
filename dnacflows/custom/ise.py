import dnacflows._payloads
class ISE:
    def __init__(self,_dnac):
       self._dnac = _dnac
       """
       dnac.custom_caller.add_ise_server.__doc__ = 
       APS that uses private API interface to create a new ISE server on DNAC
       Input is the IP address, username, password fqdn shared secret and subscription name 
       the API will create a new ISE server with TACAS enabled
       
       Receives:
        ip(string): IP Address of ISE server,
        username(string): Username from ISE to create connection
        password(string): Password for ISE connection
        fqdn(string): FQDN from ISE
        secret(string), Shared Secret
        subname(string): ISE Subscription name
        
       Returnds:
          DNAC task info
       
       """
    def add_new_ise_server(self,ip,username,password,fqdn,secret,subname):
        """

        :param ip:
        :param username:
        :param password:
        :param fqdn:
        :param secret:
        :param subname:
        :return:
        """
        payload = dnacflows._payloads.ISE()
        return self._dnac.custom_caller.call_api('POST',
                                                     'api/v1/aaa',
                                                      json=payload.new_server(iseip=ip,
                                                                                        iseuname=username,
                                                                                        isepassword=password,
                                                                                        isefqdn=fqdn,
                                                                                        secret=secret,
                                                                                        subscrinername=subname))