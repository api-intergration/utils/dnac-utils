from dnacentersdk import dnacentersdkException

class Sites(object):
	def __init__(self,dnac):
		super(Sites,self).__init__()
		self._dnac = dnac

	def get_site_id_by_name(self, site_name):
		"""
		Gets Site id from DNAC using the site name path example: if full path is
		Global/North America/New York/One Penn you need to pass North America/New York/One Penn Gloabal will be added on the en

		:param site_name:
		:return: Site ID from DNAC
		"""
		try:
			siteInfo = self._dnac.sites.get_site(name=site_name)
			return str(siteInfo["response"][0]["id"])
		except (Exception, dnacentersdkException) as e:
			return False


	def create_site_network(self, sitename):
		siteid = self.get_site_id_by_name(sitename)
