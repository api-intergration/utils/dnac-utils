class devices:
	__doc__ ="""
	      retrund DNAC device-enrichment-detials based on IP adderess

	      Receives:
	       ip(string): IP Address of device ,

	      Returnds:
	         device enrichment details
	      """
	def __init__(self, _dnac):
		self._dnac = _dnac



	def get_enrichment_details(self, ip):
		"""
        Helper function for device erichmenet details
		:param ip:
		:return:
		"""
		return self._dnac.custom_caller.call_api('GET',
												'/dna/intent/api/v1/device-enrichment-details',
												headers={
													'entity_value': ip,
													'entity_type':  'ip_address'
												}
												)
