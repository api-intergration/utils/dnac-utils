class credentials(object):
	def __init__(self):
		super(credentials, self).__init__()
	def cli(self,desc,username,password,enablepassword):
		return ({
			"settings":{
			  "cliCredential": [
               {
                  "description": desc,
                   "username": username,
                   "password": password,
                   "enablePassword": enablepassword
               }
             ]
		   }
		})

	def snmpv2read(self, desc, readcommunity):
		"""

		:param desc: Description
		:param readcommunity:
		:return: Palyload for creting device snmpv2 read credentails
		"""
		return ({
			"settings": {
				"snmpV2cRead": [
					{
						"description":    desc,
						"readCommunity":  readcommunity
					}
				]
			}
		})

	def snmpv2write(self, desc, readcommunity):
		"""

		:param desc: Description
		:param readcommunity:
		:return: Payload for creating device snmpV2 write credentails
		"""

		return ({
			"settings": {
				"snmpV2cWrite": [
					{
						"description":    desc,
						"writeCommunity":  readcommunity
					}
				]
			}
		})

	def snmpv3(self, desc, username,privacyType,privacyPassword,authType,authPassword,snmpMode):
		"""

		:param desc:
		:param username:
		:param privacyType:
		:param privacyPassword:
		:param authType:
		:param authPassword:
		:param snmpMode:
		:return:
		"""
		return ({
			"settings": {
				"snmpV2cWrite": [
					{
						 "description": desc,
               			 "username": username,
                		 "privacyType": privacyType,
                         "privacyPassword": privacyPassword,
                         "authType": authType,
                         "authPassword": authPassword,
                         "snmpMode": snmpMode
					}
				]
			}
		})

	def httpsRead(self, name, username, password, port):
		"""

		:param name:
		:param username:
		:param password:
		:param port:
		:return:
		"""
		return ({
				"settings": {
					"httpsRead": [
						{
							"name":     name,
							"username": username,
							"password": password,
							"port":     port
						}
					]
				}
			})

	def httpswrite(self, name, username, password, port):
		"""

		:param self:
		:param name:
		:param username:
		:param password:
		:param port:
		:return:
		"""
		return ({
				"settings": {
					"httpsWrite": [
						{
							"name":     name,
							"username": username,
							"password": password,
							"port":     port
						}
					]
				}
			})



