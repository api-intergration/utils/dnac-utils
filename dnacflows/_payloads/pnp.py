
class pnp(object):
    def __init__(self):
        super(pnp, self).__init__()
        """
        
        """
    def new_device_payload(self ,device):
        return (
         {
            "deviceInfo": {
            "serialNumber": device.serial,
            "name": device.serial,
            "pid": device.model_number,
            "hostname": device.name
            }

          }
	    )

    def claim_pnp_device_payload(self, deviceId, siteId):

       return (
           {
               "siteId":     siteId,
               "deviceId":   deviceId,
               "type":       "Default",
               "imageInfo":  {
                   "reload": True,
                   "skip":   False
               },
              "configInfo": {
                  "saveToStartUp":    True,
                  "connLossRollBack": True
              }
           }
       )