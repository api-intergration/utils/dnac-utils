from ..custom import *
from dnacentersdk import api
from .device_credentials import Credentials as Credentials_utils
from .devices import Devices as Device_utils
from .health import Health as Health_utils
from .networks import Networks as Networks_utils
from .sites import Sites as Sites_utils


class initDNAC(object):
    def __init__(self):
        """

        """
        self.dnac = api.DNACenterAPI()
        self.apis = apis(self.dnac)
        self.credentials = Credentials_utils(self.dnac)
        self.devices = Device_utils(self.dnac)
        self.health = Health_utils(self.dnac)
        self.networks = Networks_utils(self.dnac)
        self.sites = Sites_utils(self.dnac)
        

    def get_dnac(self):
        return self.dnac

    def get_custom_apis(self):
        return self.apis


