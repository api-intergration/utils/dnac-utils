import dnacflows.waiters
from dnacentersdk import dnacentersdkException

class Devices(object):
	def __init__(self,dnac):
		
		super(Devices, self).__init__()
		self._dnac = dnac

	def list_all_devices(self):
		try:
			return self._dnac.devices.get_device_list().response
		except (Exception, dnacentersdkException) as e:
			return f"Error reading DNAC APIs: {str(e)}"

	def add_exisitng_device_to_site(self,ip, siteName):
		try:
			siteInfo = self._dnac.sites.get_site(name=siteName)
			devices = ()

			payload = ({
				"device": [
					{
						"ip": ip
					}
				]
			})
			response = self._dnac.sites.assign_device_to_site(
				site_id=str(siteInfo["response"][0]["id"]),
				payload=payload)
			status = dnacflows.waiters.waitForTask(self._dnac, response)
			return status
		except Exception as e:
			print(str(e))
			return str(e)



