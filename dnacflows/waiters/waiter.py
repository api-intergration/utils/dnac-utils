import time

def waitForTask(dnac, task):
    try:
        # print('Wait For Task: '+ str(task))
        if 'executionStatusUrl' in task.keys():
            r = dnac.custom_caller.call_api('get', resource_path=task[
                'executionStatusUrl'])
            status = str(r['status'])
            # print('executionStatusUrl 1: '+ str(r))
            while status != 'SUCCESS' and status != 'FAILURE':
                try:
                    r = dnac.custom_caller.call_api('get',
                                                    resource_path=task[
                                                        'executionStatusUrl'])
                    status = str(r['status'])
                except Exception as e:
                    print(str(e))
                    r = {}
                    r['status'] = 'FAILURE'
                    return r
            return r
        elif 'url' in task['response'].keys():
            r = dnac.custom_caller.call_api('get',
                                            resource_path=task['response'][
                                                'url'])
            # print('URL 1:' + str(r))
            time.sleep(10)
            r = dnac.custom_caller.call_api('get',
                                            resource_path=task['response'][
                                                'url'])
            # print('URL 2:'+  str(r))
            return r['response']
        else:
            return task
    except Exception as e:
        print(str(e))
        r = {}
        r['status'] = 'FAILURE'
        return r

