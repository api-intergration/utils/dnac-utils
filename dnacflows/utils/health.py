import re

class Health(object):
	def __init__(self,dnac):
		super(Health, self).__init__()
		self._dnac = dnac
	def get_device_health(self, ident):
		"""
		
		Args:
			ident(basestring): Mac address  or Device Name

		Returns:

		"""

		macTest = re.compile(r'(?:[0-9a-fA-F]:?){12}')
		mac = re.findall(macTest, ident)
		if not mac:
			return self._dnac.devices.get_device_detail('nwDeviceName',
												  ident).response
		else:
			return self._dnac.devices.get_device_detail('macAddress',
													  ident).respnose

