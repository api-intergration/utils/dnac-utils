from ._payloads.ise import ISE as _ise_payload
from ._payloads.device_creds import credentials as _creds_payload
from ._payloads.pnp import pnp as _pnp_payload
from ._payloads.network import networkSettings as _networkSetting_payload

class Payloads(object):
	def __init__(self):
		self.ise = _ise_payload()
		self.credentials = _creds_payload()
		self.pnp = _pnp_payload
		self.network_settings = _networkSetting_payload()