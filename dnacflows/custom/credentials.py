class credentials:
	def __init__(self, _dnac):
		self._dnac = _dnac

		"""
		dnac.custom_caller.device_enrichment_details.__doc__ = 
	     Post new device crediantls into DNAC based on the payload submitted

	      Receives:
	       payload(json): JSON payload created form palyload file

	      Returnds:
	         task detials
	      """

	def create_device_credential(self, payload):
		"""
		helper function for _crete_device_credentials
		:param payload:
		:return:
		"""

		return self._dnac.custom_caller.call_api('POST',
                                                    'dna/intent/api/v1/device-credential',
                                                      json=payload)
