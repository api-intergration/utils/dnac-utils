class networkSettings(object):
    def __init__(self):
        super(networkSettings,self).__init__()

    def create_network_payload(self,payloadInfo):


        payload=({
        "settings": {
                "dhcpServer":            [
                    payloadInfo['dhcp']
                ],
                "dnsServer":             {
                    "domainName":""         ,
                    "primaryIpAddress":   "string",
                    "secondaryIpAddress": "string"
                },
                "syslogServer":          {
                    "ipAddresses":     [
                        "string"
                    ],
                    "configureDnacIP": "boolean"
                },
                "snmpServer":            {
                    "ipAddresses":     [
                        "string"
                    ],
                    "configureDnacIP": "boolean"
                },
                "netflowcollector":      {
                    "ipAddress": "string",
                    "port":      "number"
                },
                "ntpServer":             [
                    "string"
                ],
                "timezone":              "string",
                "messageOfTheday":       {
                    "bannerMessage":        "string",
                    "retainExistingBanner": "boolean"
                },
                "network_aaa":           {
                    "servers":      "string",
                    "ipAddress":    "string",
                    "network":      "string",
                    "protocol":     "string",
                    "sharedSecret": "string"
                },
                "clientAndEndpoint_aaa": {
                    "servers":      "string",
                    "ipAddress":    "string",
                    "network":      "string",
                    "protocol":     "string",
                    "sharedSecret": "string"
                }
            }
        }
        )