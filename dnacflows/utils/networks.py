from dnacflows import _payloads
from dnacentersdk import dnacentersdkException
class Networks(object):
	def __init__(self,dnac):
		super(Networks,self).__init__()
		self._dnac = dnac
	def new_ise_server (self, iseip, iseuname, isepassword, isefqdn, secret,
					   subscrinername):
		try:
			payload = _payloads.ISE.new_server(iseip, iseuname, isepassword,
											   isefqdn, secret, subscrinername)
			self._dnac.custom_caller.add_ise_server(payload)
		except (Exception, dnacentersdkException) as e:

			return f"Error reading DNAC APIs: {str(e)}"
