from .ise import ISE
from .devices import devices
from .credentials import credentials

class apis:
	def __init__(self,dnac):

		"""
		:param _dnac:
		"""
		self._dnac = dnac
		self.ISE = ISE(self._dnac)
		self.devices = devices(self._dnac)
		self.credentials = credentials(self._dnac)

